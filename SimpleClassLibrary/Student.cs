﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class Student
    {
        public String Name { get; set; }
        public String Surname { get; set; }
        public String Group { get; set; }
        public int Year { get; set; }
        public Result[] Results { get; set; }

        public Student()
        {
            this.Name = "Empty";
            this.Surname = "Empty";
            this.Group  = "Empty";
            this.Year = 0;
        }

        public Student(String Name, String Surname,String Group, int Year)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Group = Group;
            this.Year = Year;
        }
        public Student(Student foostudent)
        {
            this.Name = foostudent.Name;
            this.Surname = foostudent.Surname;
            this.Group = foostudent.Group;
            this.Year = foostudent.Year;
        }

        public Result[] ReadResults(int foo)
        {
            Results = new Result[foo];
            
            for(int i = 0; i < foo; i++)
            {
                Console.WriteLine("Введiть назву предмета");
                String subject = Console.ReadLine();

                Console.WriteLine("Введiть ПIБ викладача");
                String teacher = Console.ReadLine();

                Console.WriteLine("Введiть оцiнку за 100-бальною шкалою");
                int points = Convert.ToInt32(Console.ReadLine());

                Results[i] = new Result(subject, teacher, points);
            }

            return Results;
        }

        public int GetAveragePoints(int countofsubjects)
        {

            int average = 0;
            for (int i = 0; i < countofsubjects; i++)
            {
                average += Results[i].Points;
            }
            average /= countofsubjects;
            return average;
        }

        public String GetBestSubject(int countofsubjects)
        {
            String best = "";
            int tmp = 0;
            for(int i = 0; i<countofsubjects; i++)
            {
                if (Results[i].Points >= Results[tmp+1].Points)
                {
                    tmp = i;
                }
                best = Results[tmp].Subject;
            }
            return best;
        }

        public String GetWorstSubject(int countofsubjects)
        {
            String worst = "";
            int tmp = 0;
            for (int i = 0; i < countofsubjects; i++)
            {
                if (Results[i].Points <= Results[tmp + 1].Points)
                {
                    tmp = i;
                }
                worst = Results[tmp].Subject;
            }
            return worst;
        }

    }
}
