﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class Result
    {
        public String Subject { get; set; }
        public String Teacher { get; set; }
        public int Points { get; set; }

        public Result()
        {
            this.Subject = "Empty";
            this.Teacher = "Empty";
            this.Points = 0;
        }

        public Result(String Subject, String Teacher, int Points)
        {
            this.Subject = Subject;
            this.Teacher = Teacher;
            this.Points = Points;
        }
        public Result(Result fooresult)
        {
            this.Subject = fooresult.Subject;
            this.Teacher = fooresult.Teacher;
            this.Points = fooresult.Points;
        }
    }
}
