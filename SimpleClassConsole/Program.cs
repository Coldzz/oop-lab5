﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleClassLibrary;

namespace SimpleClassConsole
{
    class Program
    {
        static Student[] ReadStudentsArray(int foo)
        {
            Student[] students = new Student[foo];

            for (int i = 0; i < foo; i++)
            {
                Console.WriteLine("Введiть Iм'я студента {0}", i);
                String name = Console.ReadLine();

                Console.WriteLine("Введiть Прiзвище студента {0}", i);
                String surname = Console.ReadLine();

                Console.WriteLine("Введiть групу студента {0}", i);
                String group = Console.ReadLine();

                Console.WriteLine("Введiть рiк на якому навчається студент {0}", i);
                int year = Convert.ToInt32(Console.ReadLine());


                students[i] = new Student(name, surname, group, year);

                Console.WriteLine("Введiть кiлькiсть предметiв сесiї:");
                int number = Convert.ToInt32(Console.ReadLine());
                students[i].ReadResults(number);


            }
            return students;
        }

        static void PrintStudents(Student[] students)
        {
            for (int i = 0; i < students.Length; i++)
            {
                Student student = students[i];

                Console.WriteLine("Iм'я: {0}, Прiзвище: {1}, Група: {2}, Рiк навчання: {3}", student.Name, student.Surname, student.Group, student.Year);
            }
        }

        static void GetStudentsInfo(Student[] students)
        {
            Student max = students[0];
            Student min = students[0];

            for (int i = 1; i < students.Length; i++)
            {
                if (students[i].GetAveragePoints(min.Results.Length) < min.GetAveragePoints(min.Results.Length))
                {
                    min = students[i];
                }

                if (students[i].GetAveragePoints(min.Results.Length) > max.GetAveragePoints(min.Results.Length))
                {
                    max = students[i];
                }
            }

            Console.WriteLine("Найменший балл: {0}", min.GetAveragePoints(min.Results.Length));
            Console.WriteLine("Найбiльший балл: {0}", max.GetAveragePoints(min.Results.Length));
            //student.GetAveragePoints(student.Results.Length)
            //if (student.GetAveragePoints(student.Results.Length) < student.GetAveragePoints(student.Results.Length)) { 
        }

        static void SortStudentsByPoints(Student[] students)
        {
            students = students.OrderBy(obj => obj.GetAveragePoints(obj.Results.Length)).ToArray();

            Console.WriteLine("Сортування за спаданням:");
            PrintStudents(students);
        }

        static void SortStudentsByName(Student[] students)
        {
            students = students.OrderBy(obj => obj.Name).ToArray();

            Console.WriteLine("Сортування за спаданням:");
            PrintStudents(students);
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Введiть кiлькiсть студентiв:");
            int foo = Convert.ToInt32(Console.ReadLine());
            Student[] students = ReadStudentsArray(foo);

            PrintStudents(students);

            while (true)
            {
                Console.WriteLine("Оберiть пункт меню:");
                Console.WriteLine("1. Переглянути результати сесiї");
                Console.WriteLine("2. Сортувати за iм'ям");
                Console.WriteLine("3. Сортувати за оцiнками");
                Console.WriteLine("4. Вихід");

                int choose = Convert.ToInt32(Console.ReadLine());

                switch (choose)
                {
                    case 1:
                        GetStudentsInfo(students);
                        break;

                    case 2:
                        SortStudentsByName(students);
                        break;

                    case 3:
                        SortStudentsByPoints(students);
                        break;
                }
                if (choose == 4)
                {
                    break;
                }
            }
        }
    }
}
